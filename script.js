//Get post
const getData = () => {
    fetch('https://jsonplaceholder.typicode.com/users')
        .then((response) => {
            return response.json();
        }).then((data) => {
            let posts = '';
            data.forEach((post) => {
                // posts +=`<div>
                //     <h2>${post.name}</h2>
                //     <p>${post.username}</p>
                //     <p>${post.email}</p>
                // </div>`
                posts += `
                <div class="card" style="width: 18rem;">
                    <img src="./images/icon.png" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Name: ${post.name}</h5>
                        <p class="card-text">Email: ${post.email}</p>
                        <p class="card-text">Company: ${post.company.name}</p>
                        <p class="card-text">Phone: ${post.phone}</p>
                    </div>
                </div>`
            });
            document.querySelector('#output').innerHTML = posts;
        })
        .catch((err) => {
            console.log('rejected')
        })
}

// //Add Post
//e = event object -> give all the details of that event Example: mouse ->click, keyboard->keyup, form -> submit
const addPost = (e) => {
    //preventDefault -> iprpevent and dapat mangyari
    e.preventDefault();
    let name = document.querySelector('#name').value;
    let username = document.querySelector('#username').value;
    let email = document.querySelector('#email').value;
    let street = document.querySelector('#street').value;
    let suite = document.querySelector('#suite').value;
    let city = document.querySelector('#city').value;
    let zipcode = document.querySelector('#zipcode').value;
    let lat = document.querySelector('#lat').value;
    let lang = document.querySelector('#lang').value;
    let phone = document.querySelector('#phone').value;
    let website = document.querySelector('#website').value;
    let cname = document.querySelector('#cname').value;
    let catchPhrase = document.querySelector('#catchPhrase').value;
    let bs = document.querySelector('#bs').value;


    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain',
            'Content-type': 'application/json'
        },
        body: JSON.stringify({
            //title at body is galing dun sa taas na naka let
            name: name,
            username: username,
            email: email,
            address: {
                street: street,
                suite: suite,
                city: city,
                zipcode: zipcode,
                geo : {
                    lat: lat,
                    lang: lang,
                }
            },
            phone: phone,
            website: website,
            company: {
                cname: cname,
                catchPhrase: catchPhrase,
                bs: bs
            }
        }) 
    })
    .then(response => {
        return response.json()
    })
    .then(data => console.log(data))

}




const btn1 = document.querySelector('#btn1');
btn1.addEventListener('click', getData);

const addForm = document.querySelector('#addPost');
addForm.addEventListener('submit', addPost);